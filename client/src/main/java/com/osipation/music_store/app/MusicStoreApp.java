package com.osipation.music_store.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicStoreApp {
    public static void main(String[] args) {
        SpringApplication.run(MusicStoreApp.class, args);
    }
}
