package com.osipation.music_store.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.osipation.music_store")
public class AppConfig {
}
